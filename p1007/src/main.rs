use std::io::prelude::*;
use std::io::BufReader;
use std::cmp::Ordering;

type Word = Vec<bool>;

struct Recoverer {
    word_lenght: usize,
    divider: usize,
}

impl Recoverer {
    fn new(word_lenght: usize) -> Self {
        Self {
            word_lenght,
            divider: word_lenght + 1,
        }
    }
    fn recover(&self, word: &mut Word) {
        let (total_sum, one_count) = (1..).zip(word.iter()).fold((0, 0), |(sum, one_count), (i, b)| (sum + i * *b as usize, one_count + *b as usize));
        if total_sum % self.divider == 0 {
            if word.len() < self.word_lenght {
                word.push(false);
            } else if word.len() > self.word_lenght {
                word.pop();
            }
            return;
        }

        match word.len().cmp(&self.word_lenght) {
            Ordering::Equal => self.recover_replacement(word, total_sum),
            Ordering::Greater => self.recover_insertion(word, total_sum, one_count), 
            Ordering::Less => self.recover_removal(word, total_sum, one_count),
        }
    }
    fn recover_replacement(&self, word: &mut Word, total_sum: usize) {
        word[total_sum % self.divider - 1] = false;
    }
    fn recover_removal(&self, word: &mut Word, total_sum: usize, one_count: usize) {
        let mut on_count_right = one_count;
        /* |0101 */
        for (i, byte_is_on) in (1..).zip(word.iter()) {
            let total_sum_new = total_sum + on_count_right;
            if total_sum_new % self.divider == 0 {
                word.insert(i-1, false);
                return;
            }
            if (total_sum_new + i) % self.divider == 0 {
                word.insert(i-1, true);
                return;
            }
            if *byte_is_on {
                on_count_right -= 1;
            }
        }
        word.push(true);
    }
    fn recover_insertion(&self, word: &mut Word, total_sum: usize, one_count: usize) {
        let mut on_count_right = one_count;
        for (i, byte_is_on) in (1..).zip(word.iter()) {
            let total_sum_new = if *byte_is_on {
                on_count_right -= 1;
                total_sum - on_count_right - i
            } else {
                total_sum - on_count_right
            };
            if total_sum_new % self.divider == 0 {
                word.remove(i-1);
                return;
            }
        }
        panic!("Can't fix insertion! {:?}", word);
    }

}

fn byte_to_bool(byte: char) -> bool {
    match byte {
        '0' => false,
        _ => true,
    }
}

fn main() -> std::io::Result<()> {
    let input = std::io::stdin();
    let reader = BufReader::new(input);
    let mut lines = reader.lines();
    let lenght: usize = lines.next().unwrap()?.trim_end().parse().unwrap();
    let recoverer = Recoverer::new(lenght);
    for line in lines {
        if let Ok(word) = line {
            if word.is_empty() {
                continue;
            }
            let mut word: Vec<bool> = word.trim_end().chars().map(|ch| byte_to_bool(ch)).collect();
            recoverer.recover(&mut word);
            for b in word.iter() {
                print!("{}", *b as usize);
            }
            println!("");
        } else {
            break;
        }
    }
    Ok(())
}

