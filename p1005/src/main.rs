use std::io::prelude::*;
use std::io::BufReader;
use std::cmp::Ordering;

fn sums_of_subsets(set: &[u32]) -> Vec<u32> {
    let subsets = 1 << set.len();
    let mut sums: Vec<u32> = Vec::with_capacity(subsets);
    for i in 1..subsets {
        let mut sum = 0;
        for j in 0..set.len() {
            if i & (1 << j) != 0 {
                sum += set[j];
            }
        }
        if let Err(idx) = sums.binary_search(&sum) {
            sums.insert(idx, sum);
        }
    }
    sums
}

fn solve(weights: &[u32]) -> u32 {
    let weights = {
        let mut weights = weights.to_vec();
        weights.sort();
        weights
    };
    match weights.len() {
        1 => return weights[0],
        2 => return weights[1] - weights[0],
        _ => (),
    }
    let total_sum: u32 = weights.iter().sum();
    let half_sum = total_sum / 2;
    let (set1, set2) = weights.split_at(weights.len() / 2);
    let sums1 = sums_of_subsets(set1);
    let sums2 = sums_of_subsets(set2);
    let mut sums1_iter = sums1.iter().rev();
    let mut sums2_iter = sums2.iter();
    let mut sum1 = sums1_iter.next().unwrap();
    let mut sum2 = sums2_iter.next().unwrap();
    let mut lower_bound = 0;
    loop {
        let sum = sum1 + sum2;
        match sum.cmp(&half_sum) {
            Ordering::Greater => match sums1_iter.next() {
                Some(val) => sum1 = val,
                None => break,
            },
            Ordering::Equal => {
                lower_bound = half_sum;
                break;
            },
            Ordering::Less => {
                if lower_bound < sum {
                    lower_bound = sum;
                }
                match sums2_iter.next() {
                    Some(val) => sum2 = val,
                    None => break,
                }
            },
        }
    };
    if lower_bound != 0 {
        return total_sum - 2 * lower_bound
    }

    let mut upper_bound = 0;
    for w in weights {
        upper_bound += w;
        if upper_bound > half_sum {
            break;
        }
    }
    upper_bound * 2 - total_sum
}

fn main() -> std::io::Result<()> {
    let input = std::io::stdin();
    let reader = BufReader::new(input);
    let mut lines = reader.lines();
    let _stones_number: usize = lines.next().unwrap()?.parse().unwrap();
    let weights: Vec<_> = lines.next().unwrap()?.split_whitespace()
        .map(|x| x.parse().unwrap()).collect();
    let answer = solve(&weights);
    println!("{}", answer);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn one_element() {
        let weights = [10];
        let answer = solve(&weights);
        assert_eq!(answer, 10);
    }
    #[test]
    fn two_elements() {
        let weights = [10, 101];
        let answer = solve(&weights);
        assert_eq!(answer, 91);
    }
    #[test]
    fn more_than_two_elements() {
        let weights = [10, 101, 1010];
        let answer = solve(&weights);
        assert_eq!(answer, 899);
    }
    #[test]
    fn test1() {
        let weights = [36, 25, 12, 10, 8, 7, 1];
        let answer = solve(&weights);
        assert_eq!(answer, 1);
    }
    #[test]
    fn test2() {
        let weights = [1, 1, 1, 1, 1];
        let answer = solve(&weights);
        assert_eq!(answer, 1);
    }
    #[test]
    fn test3() {
        let weights = [6, 7, 9, 13, 18, 24, 31, 50];
        let answer = solve(&weights);
        assert_eq!(answer, 0);
    }
    #[test]
    fn test4() {
        let weights = [1, 4, 5, 6, 7, 9];
        let answer = solve(&weights);
        assert_eq!(answer, 0);
    }
    #[test]
    fn test5() {
        let weights = [6, 6, 6, 5, 4, 3, 2];
        let answer = solve(&weights);
        assert_eq!(answer, 0);
    }
    #[test]
    fn test6() {
        let weights = [100, 1000, 1001];
        let answer = solve(&weights);
        assert_eq!(answer, 99);
    }
}
