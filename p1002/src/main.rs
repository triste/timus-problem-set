use std::io::prelude::*;
//use std::fs::File;
use std::io::BufReader;
const ALPHABET_LENGHT: usize = 10;

use std::default::Default;

type NodeIdx = u32;
const NODE_IDX_ROOT: NodeIdx = 0;

type WordIdx = u16;

#[derive(Default, Debug)]
struct Node {
    childes: [NodeIdx; ALPHABET_LENGHT],
    go: [Option<NodeIdx>; ALPHABET_LENGHT],
    suflink: Option<NodeIdx>,
    compressed_suflink: Option<NodeIdx>,
    parent: NodeIdx,
    word_index: Option<WordIdx>,
    digit: u8,
}

#[derive(Default, Debug)]
struct Trie {
    nodes: Vec<Node>,
}

impl Trie {
    fn get_link(&mut self, node_index: NodeIdx, digit: u8) -> NodeIdx {
        let idx = node_index as usize;
        let digit = digit as usize;
        if let Some(link) = self.nodes[idx].go[digit] {
            return link;
        }
        let link = if self.nodes[idx].childes[digit] != 0 {
            self.nodes[idx].childes[digit]
        } else if node_index == NODE_IDX_ROOT {
            NODE_IDX_ROOT
        } else {
            let suflink_index = self.get_suflink(node_index);
            self.get_link(suflink_index, digit as u8)
        };
        self.nodes[idx].go[digit] = Some(link);
        link
    }

    fn get_suflink(&mut self, node_index: NodeIdx) -> NodeIdx {
        let idx = node_index as usize;
        if let Some(suflink) = self.nodes[idx].suflink {
            return suflink;
        }
        let suflink = if node_index == NODE_IDX_ROOT || self.nodes[idx].parent == NODE_IDX_ROOT {
            NODE_IDX_ROOT
        } else {
            let suflink = self.get_suflink(self.nodes[idx].parent);
            self.get_link(suflink, self.nodes[idx].digit)
        };
        self.nodes[idx].suflink = Some(suflink as u32);
        suflink
    }
    fn get_up(&mut self, node_index: NodeIdx) -> NodeIdx {
        let idx = node_index as usize;
        if let Some(compressed_suflink) = self.nodes[idx].compressed_suflink {
            return compressed_suflink;
        }
        let suflink = self.get_suflink(node_index);
        let compressed_suflink = if self.nodes[suflink as usize].word_index != None {
            suflink
        } else if suflink == NODE_IDX_ROOT {
            NODE_IDX_ROOT
        } else {
            self.get_up(suflink)
        };
        self.nodes[idx].compressed_suflink = Some(compressed_suflink);
        compressed_suflink
    }
}

const DICTIONARY_SIZE_MAX: usize = 50000;
const INPUT_BUFFER_LENGHT_MAX: usize = 300000;

#[derive(Default, Clone)]
struct SliceCell {
    prefix_cell: Option<usize>,
    suffix_word_idx: WordIdx,
    total_slices: usize,
}

impl Trie {
    fn with_capacity(capacity: usize) -> Self {
        let root = Default::default();
        let mut nodes = Vec::with_capacity(capacity * (INPUT_BUFFER_LENGHT_MAX / DICTIONARY_SIZE_MAX));
        nodes.push(root);
        Trie { nodes }
    }
    fn insert(&mut self, word: &String, index: u16) {
        let mut current_node_idx: u32 = 0;
        for c in word.chars() {
            let digit = match c {
                'i' | 'j' => 1,
                'a' | 'b' | 'c' => 2,
                'd' | 'e' | 'f' => 3,
                'g' | 'h' => 4,
                'k' | 'l' => 5,
                'm' | 'n' => 6,
                'p' | 'r' | 's' => 7,
                't' | 'u' | 'v' => 8,
                'w' | 'x' | 'y' => 9,
                'o' | 'q' | 'z' => 0,
                _ => panic!("Wrong char!"),
            };
            let next_idx = self.nodes[current_node_idx as usize].childes[digit];
            let next_node_idx = if next_idx == 0 {
                let next_node_idx = self.nodes.len() as u32;
                let new_node = Node {
                    parent: current_node_idx,
                    digit: digit as u8,
                    ..Default::default()
                };
                self.nodes[current_node_idx as usize].childes[digit] = next_node_idx;
                self.nodes.push(new_node);
                next_node_idx
            } else {
                self.nodes[current_node_idx as usize].childes[digit] as u32
            };
            current_node_idx = next_node_idx;
        }
        self.nodes[current_node_idx as usize].word_index = Some(index);
    }
    fn process(&mut self, number: &String) -> Option<Vec<Vec<u16>>> {
        let mut node_idx = NODE_IDX_ROOT;
        let mut occurrences: Vec<Vec<u16>> = Vec::new();
        for (_, c) in number.chars().enumerate() {
            let mut occur: Vec<u16> = Vec::new();
            let c: u8 = c.to_digit(10).unwrap() as u8;
            node_idx = self.get_link(node_idx, c);
            if node_idx == NODE_IDX_ROOT {
                return None;
            }
            if let Some(word) = self.nodes[node_idx as usize].word_index {
                occur.push(word);
            }
            let mut term = self.get_up(node_idx);
            while term != NODE_IDX_ROOT {
                let word = self.nodes[term as usize].word_index.unwrap();
                occur.push(word);
                term = self.get_up(term);
            }
            occurrences.push(occur);
        }
        return Some(occurrences);
    }
}

fn main() -> std::io::Result<()> {
    //let input = File::open("input.txt")?;
    let input = std::io::stdin();
    let reader = BufReader::new(input);
    let mut lines_iter = reader.lines();
    loop {
        let number = lines_iter.next().unwrap()?;
        if number == "-1" {
            break;
        }
        let word_count: usize = lines_iter.next().unwrap()?.parse().unwrap();
        let mut dict: Vec<String> = Vec::with_capacity(word_count);
        let mut trie = Trie::with_capacity(word_count);
        for i in 0..word_count {
            let word = lines_iter.next().unwrap()?;
            trie.insert(&word, i as u16);
            dict.push(word);
        }
        if let Some(occurrences) = trie.process(&number) {
            let mut map: Vec<SliceCell> = vec![Default::default(); occurrences.len()];
            for (i, occ) in occurrences.iter().enumerate() {
                if occ.is_empty() {
                    continue;
                }
                for word_index in occ {
                    let word_len = dict[*word_index as usize].len();
                    if word_len > i {
                        let slice_cell = SliceCell {
                            prefix_cell: None,
                            suffix_word_idx: *word_index,
                            total_slices: 1,
                        };
                        map[i] = slice_cell;
                    } else if map[i - word_len].total_slices != 0 {
                        let current_val = map[i].total_slices;
                        let prefix_cell_idx = i - word_len;
                        let next_val = map[prefix_cell_idx].total_slices + 1;
                        if current_val == 0 || next_val < current_val {
                            map[i] = SliceCell {
                                prefix_cell: Some(prefix_cell_idx),
                                suffix_word_idx: *word_index,
                                total_slices: next_val,
                            };
                        }
                    }
                }
            }
            let mut word_indexes: Vec<WordIdx> = Vec::new();
            let mut slice = map.last().unwrap();
            if slice.total_slices == 0 {
                println!("No solution.");
                continue;
            }
            loop {
                word_indexes.push(slice.suffix_word_idx);
                if let Some(prefix_cell_idx) = slice.prefix_cell {
                    slice = &map[prefix_cell_idx];
                } else {
                    break;
                }
            }
            let mut word_idx_it = word_indexes.into_iter().rev();
            print!("{}", dict[word_idx_it.next().unwrap() as usize]);
            while let Some(idx) = word_idx_it.next() {
                print!(" {}", dict[idx as usize]);
            }
            println!("");
        } else {
            println!("No solution.");
        }
    }
    Ok(())
}
